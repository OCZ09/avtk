<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    

	return $this->view->render($response, 'index.latte');
});

$app->get('/api/mereni', function (Request $request, Response $response, array $args) {

    $stmt = $this->db->query("SELECT * FROM dochazka");

    return $response->withJson($stmt->fetchAll());

})->setName("apis");


$app->get('/pub', function (Request $request, Response $response, array $args) {

    $stmt = $this->db->query('SELECT * FROM rooms');

    return $response->withJson($stmt->fetchALL());

});

$app->post('/pub', function (Request $request, Response $response, array $args) {

    $data = $request->getParsedBody();

    try {
        $stmt = $this->db->prepare("INSERT INTO dochazka (cas, typ, popis, stav) VALUES (NOW(),:t, :p, :s)");
        $stmt->bindValue(':t', $data['doch']);
        $stmt->bindValue(':p', $data['popis']);
        $stmt->bindValue(':s', $data['stav']);
        $stmt->execute();

        $id = $this->db->lastInsertId("mereni_vyska_id_seq");

        return $response->withJson([
            'id' => $id
        ], 201);

    } catch (Exception $ex) {
        return $response->withJson([
            'err' => $ex->getMessage()],500);
    }
});

$app->get('/dochazka', function (Request $request, Response $response, array $args) {
    $p = $request->getQueryParam('id');
    try{
        if(empty($p['filtr'])){

            $stmt = $this->db->query("SELECT * FROM dochazka");
            $tplVars['dochazka'] = $stmt->fetchALL();

        }else{
            $stmt = $this->db->prepare("SELECT * FROM dochazka WHERE id ILIKE :id OR typ ILIKE :typ OR popis ILIKE :po");
            $stmt->bindValue(':id', '%' . $p['filtr'] . '%');
            $stmt->bindValue(':typ', '%' . $p['filtr'] . '%');
            $stmt->bindValue(':po', '%' . $p['filtr'] . '%');
            $stmt->execute();

            $tplVars['dochazka'] = $stmt->fetchALL();
        }
        
    }catch (Exception $ex){
        exit("NO");
    }
    return $this->view->render($response, 'dochazka.latte',$tplVars);
})->setName('dochazka');

$app->post('/dochazka', function (Request $request, Response $response, array $args) {

})->setName('dochazka');
